package com.ad2pro.spectra.adw.dfp.services;

import com.ad2pro.spectra.adw.dfp.DfpApiService;
import com.ad2pro.spectra.adw.dfp.domain.AdUnits;
import com.google.api.ads.admanager.axis.factory.AdManagerServices;
import com.google.api.ads.admanager.axis.utils.v201905.StatementBuilder;
import com.google.api.ads.admanager.axis.v201905.AdUnit;
import com.google.api.ads.admanager.axis.v201905.AdUnitPage;
import com.google.api.ads.admanager.axis.v201905.InventoryServiceInterface;
import com.google.api.ads.admanager.lib.client.AdManagerSession;
import com.google.api.ads.common.lib.conf.ConfigurationLoadException;
import com.google.api.ads.common.lib.exception.OAuthException;
import com.google.api.ads.common.lib.exception.ValidationException;
import lombok.extern.slf4j.Slf4j;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class InventoryService extends DfpApiService {
    private String networkCode;

    public InventoryService(String networkCode) {
        this.networkCode = networkCode;
    }

    public List<AdUnits> fetchAllAdUnits(String adUnitName, String parentAdUnitId) throws ValidationException, OAuthException, ConfigurationLoadException, RemoteException {
        AdManagerSession session = super.getSession(networkCode);
        AdManagerServices adManagerServices = new AdManagerServices();
        InventoryServiceInterface inventoryService = adManagerServices.get(session, InventoryServiceInterface.class);
        String statement = "name = :name";
        if (parentAdUnitId != null) {
            statement += " and  parentId = '" + parentAdUnitId + "'";
        }
        StatementBuilder statementBuilder = new StatementBuilder()
                .where(statement)
                .orderBy("id ASC")
                .withBindVariableValue("name", adUnitName);
        log.info("DFP AdUnit details for networkCode:{},  AdUnit :{}",networkCode, adUnitName);
        AdUnitPage adUnitPage = inventoryService.getAdUnitsByStatement(statementBuilder.toStatement());
        List<AdUnits> fetchedAdUnits = new ArrayList<>();
        for (AdUnit adunit : adUnitPage) {
            AdUnits data = new AdUnits();
            data.setId(adunit.getId());
            data.setName(adunit.getName());
            fetchedAdUnits.add(data);
        }
        return fetchedAdUnits;
    }

    public List<AdUnits> fetchAllAdUnits(String adUnitName) throws ValidationException, OAuthException, ConfigurationLoadException, RemoteException {
        return this.fetchAllAdUnits(adUnitName, null);
    }

//    public static void main(String[] args) {
//        try {
//            InventoryService inventoryService = new InventoryService("154725070");
////            inventoryService.fetchAllAdUnits("www.dailyecho.co.uk", (long) 154577070);
//            inventoryService.fetchAllAdUnits("news", "154577070");
//        } catch (Exception e) {
//            System.out.println("error");
//            System.out.println(e);
//        }
//    }
}
