package com.ad2pro.spectra.adw.dfp;

import com.ad2pro.spectra.adw.IAdServerApiService;
import com.ad2pro.spectra.adw.auth.RefreshTokenService;
import com.google.api.ads.admanager.lib.client.AdManagerSession;
import com.google.api.ads.common.lib.conf.ConfigurationLoadException;
import com.google.api.ads.common.lib.exception.OAuthException;
import com.google.api.ads.common.lib.exception.ValidationException;

public class DfpApiService implements IAdServerApiService {
    public AdManagerSession getSession(String networkCode) throws ValidationException, OAuthException, ConfigurationLoadException {
        RefreshTokenService token = RefreshTokenService.getInstance();
        return token.getToken(networkCode);
    }
}
