package com.ad2pro.spectra.adw.dfp.domain;

import lombok.Data;

@Data
public class Advertiser {
    private long id;
    private long appliedTeamIds[];
    private String name;
    private String type;
}
