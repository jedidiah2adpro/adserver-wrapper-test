package com.ad2pro.spectra.adw.dfp.domain;

import lombok.Data;

@Data
public class AdUnits {
    private String name;
    private String id;
}
