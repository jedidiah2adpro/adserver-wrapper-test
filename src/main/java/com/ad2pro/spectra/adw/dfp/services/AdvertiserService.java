package com.ad2pro.spectra.adw.dfp.services;

import com.ad2pro.spectra.adw.dfp.DfpApiService;
import com.google.api.ads.admanager.axis.factory.AdManagerServices;
import com.google.api.ads.admanager.axis.utils.v201905.StatementBuilder;
import com.google.api.ads.admanager.axis.v201905.*;
import com.google.api.ads.admanager.lib.client.AdManagerSession;
import com.google.api.ads.common.lib.conf.ConfigurationLoadException;
import com.google.api.ads.common.lib.exception.OAuthException;
import com.google.api.ads.common.lib.exception.ValidationException;
import com.ad2pro.spectra.adw.dfp.domain.Advertiser;
import lombok.extern.slf4j.Slf4j;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;


@Slf4j
public class AdvertiserService extends DfpApiService {
    private String networkCode;


    public AdvertiserService(String networkCode) {
        this.networkCode = networkCode;
    }

    public List<Advertiser> getAdvertiserByName(String advertiserName) throws ValidationException, OAuthException, ConfigurationLoadException, RemoteException, NullPointerException {
        AdManagerSession session = super.getSession(networkCode);
        AdManagerServices adManagerServices = new AdManagerServices();
        CompanyServiceInterface companyServiceInterface = adManagerServices.get(session, CompanyServiceInterface.class);
        StatementBuilder statementBuilder = new StatementBuilder()
                .where("type = :type and name = :name")
                .orderBy("id ASC")
                .limit(1)
                .withBindVariableValue("type", CompanyType.ADVERTISER.toString())
                .withBindVariableValue("name", advertiserName);
        log.info("DFP Advertiser details for networkCode:{},  advertiser :{}",networkCode, advertiserName);
        CompanyPage companyPages = companyServiceInterface.getCompaniesByStatement(statementBuilder.toStatement());
        List<Advertiser> foundAdvertisers = new ArrayList<>();
        for (Company company: companyPages) {
            Advertiser data = new Advertiser();
            data.setName(company.getName());
            data.setAppliedTeamIds(company.getAppliedTeamIds());
            data.setType(company.getType().toString());
            data.setId(company.getId());
            foundAdvertisers.add(data);
        }
        return foundAdvertisers;
    }

    public List<Advertiser> createAdvertiser(String advertiserName, String teamName) throws ValidationException, OAuthException, ConfigurationLoadException, RemoteException{
        AdManagerSession session = super.getSession(networkCode);
        AdManagerServices adManagerServices = new AdManagerServices();
        CompanyServiceInterface companyServiceInterface = adManagerServices.get(session, CompanyServiceInterface.class);
        TeamService teamService = new TeamService(networkCode);
        TeamPage teamDetails = teamService.getTeamByName(teamName);
        Company advertiser = new Company();
        if(teamDetails.getTotalResultSetSize() > 0) {
            long teamId[] = new long[teamDetails.getTotalResultSetSize()];
            int i = 0;
            for (Team team: teamDetails) {
                teamId[i] = team.getId();
                i++;
            }
            teamId[0] = teamDetails.getResults(0).getId();
            advertiser.setAppliedTeamIds(teamId);
        }
        advertiser.setName(advertiserName);
        advertiser.setType(CompanyType.ADVERTISER);
        log.info("DFP Advertiser creation details for networkCode:{},  advertiser :{}",networkCode, advertiserName);
        Company[] companies = companyServiceInterface.createCompanies(new Company[]{advertiser});
        List<Advertiser> createdAdvertisers = new ArrayList<>();
        for (Company company: companies) {
            Advertiser data = new Advertiser();
            data.setName(company.getName());
            data.setAppliedTeamIds(company.getAppliedTeamIds());
            data.setType(company.getType().toString());
            data.setId(company.getId());
            createdAdvertisers.add(data);
        }
        return createdAdvertisers;
    }

}
