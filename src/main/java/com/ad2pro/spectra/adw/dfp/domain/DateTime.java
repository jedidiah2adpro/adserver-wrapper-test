package com.ad2pro.spectra.adw.dfp.domain;

import lombok.Data;

@Data
public class DateTime {
    private Date date;
    private int hour;
    private int minute;
    private int second;
    private String timeZoneId;
}
