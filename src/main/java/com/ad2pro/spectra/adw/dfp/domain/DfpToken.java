package com.ad2pro.spectra.adw.dfp.domain;

import com.google.api.ads.admanager.lib.client.AdManagerSession;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DfpToken {
    private AdManagerSession adManagerSession;
    private LocalDateTime tokenIssuedOn;
}
