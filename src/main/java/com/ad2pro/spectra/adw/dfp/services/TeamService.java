package com.ad2pro.spectra.adw.dfp.services;

import com.ad2pro.spectra.adw.dfp.DfpApiService;
import com.google.api.ads.admanager.axis.factory.AdManagerServices;
import com.google.api.ads.admanager.axis.utils.v201905.StatementBuilder;
import com.google.api.ads.admanager.axis.v201905.TeamPage;
import com.google.api.ads.admanager.axis.v201905.TeamServiceInterface;
import com.google.api.ads.admanager.lib.client.AdManagerSession;
import com.google.api.ads.common.lib.conf.ConfigurationLoadException;
import com.google.api.ads.common.lib.exception.OAuthException;
import com.google.api.ads.common.lib.exception.ValidationException;
import lombok.extern.slf4j.Slf4j;

import java.rmi.RemoteException;

@Slf4j
public class TeamService extends DfpApiService {
    private String networkCode;

    public TeamService(String networkCode) { this.networkCode = networkCode; }

    public TeamPage getTeamByName(String teamName) throws ValidationException, OAuthException, ConfigurationLoadException, RemoteException {
        AdManagerSession session = super.getSession(networkCode);
        AdManagerServices adManagerServices = new AdManagerServices();
        TeamServiceInterface teamService = adManagerServices.get(session, TeamServiceInterface.class);
        StatementBuilder statementBuilder = new StatementBuilder()
                .where("name = :name")
                .orderBy("id ASC")
                .withBindVariableValue("name", teamName);
        log.info("DFP Team details for networkCode:{},  teamName :{}",networkCode, teamName);
        TeamPage page = teamService.getTeamsByStatement(statementBuilder.toStatement());
        return page;
    }

//
//    public static void main(String[] args) {
//        try {
//            TeamService teamService = new TeamService("154725070");
//            teamService.getTeamByName("2AdPro Preview");
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }
}
