package com.ad2pro.spectra.adw.dfp.domain;

import lombok.Data;

@Data
public class Date {
    private int year;
    private int month;
    private int day;
}
