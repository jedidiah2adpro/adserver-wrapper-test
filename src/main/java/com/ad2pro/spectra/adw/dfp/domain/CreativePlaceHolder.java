package com.ad2pro.spectra.adw.dfp.domain;

import lombok.Data;

@Data
public class CreativePlaceHolder {
    long width;
    long height;
}
