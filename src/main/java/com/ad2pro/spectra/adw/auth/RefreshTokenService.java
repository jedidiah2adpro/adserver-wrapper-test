package com.ad2pro.spectra.adw.auth;

import com.ad2pro.spectra.adw.dfp.domain.DfpToken;
import com.ad2pro.spectra.adw.utils.Constants;
import com.google.api.ads.admanager.lib.client.AdManagerSession;
import com.google.api.ads.common.lib.auth.OfflineCredentials;
import com.google.api.ads.common.lib.conf.ConfigurationLoadException;
import com.google.api.ads.common.lib.exception.OAuthException;
import com.google.api.ads.common.lib.exception.ValidationException;
import com.google.api.client.auth.oauth2.Credential;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class RefreshTokenService {

    private static RefreshTokenService token;
    private Map<String,DfpToken> dfpTokenVsExpiryTime = new HashMap<>();

    private RefreshTokenService() {

    }

    public static RefreshTokenService getInstance() {
        if(token != null) return token;
        synchronized (RefreshTokenService.class) {
            if(token == null)
                token = new RefreshTokenService();
        }
        return token;
    }

    private void getRefreshToken(String networkCode) throws ValidationException, OAuthException, ConfigurationLoadException {
        Credential oAuth2Crendtial = new OfflineCredentials.Builder()
                .forApi(OfflineCredentials.Api.AD_MANAGER)
                .withClientSecrets(Constants.dfpClientId, Constants.dfpClientSecret)
                .withRefreshToken(Constants.dfpRefreshToken)
                .build()
                .generateCredential();
        AdManagerSession adManagerSession = new AdManagerSession.Builder()
                .fromFile()
                .withApplicationName(Constants.dfpApplicationName)
                .withOAuth2Credential(oAuth2Crendtial)
                .withNetworkCode(networkCode)
                .build();
        DfpToken dfpToken = new DfpToken();
        dfpToken.setAdManagerSession(adManagerSession);
        dfpToken.setTokenIssuedOn(LocalDateTime.now());
        dfpTokenVsExpiryTime.put(networkCode, dfpToken);
        log.info("Admanager session Generated for network :{}", networkCode);
    }

    public AdManagerSession getToken(String networkCode) throws ValidationException, OAuthException, ConfigurationLoadException{
        if(dfpTokenVsExpiryTime.isEmpty()) {
            getRefreshToken(networkCode);
        }
        return dfpTokenVsExpiryTime.get(networkCode).getAdManagerSession();
    }
//
//    public static void main(String[] args) {
//        Token token = Token.getInstance();
//        Token token1 = Token.getInstance();
//        try {
//            System.out.println("first instance");
//            token.getToken("154725070");
//            System.out.println("second");
//            token1.getToken("154725070");
////            System.out.println(token1.dfpTokenVsExpiryTime);
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//
//    }
}
